
# Leaflet.FeatureGroup.OGCAPI

Leaflet plugin to load & display vector data from a OGC API Features service.


This LeafletJS plugin adds a new class, `L.FeatureGroup.OGCAPI`, subclass of
[`L.GeoJSON`](http://leafletjs.com/reference.html#geojson). Every time
the map viewport (center/zoom) changes, the data will be reloaded.

## Demo

https://ivansanchez.gitlab.io/leaflet.featuregroup.ogcapi/demo.html

### Loading the plugin

Load the plugin by adding the appropriate `<script>` HTML tag, for example:

```html
<script src='https://unpkg.com/leaflet-featuregroup-ogcapi@0.1.0/Leaflet.FeatureGroup.OGCAPI.js'></script>
```

The plugin is available as an NPM package with the name
`leaflet-featuregroup-ogcapi`; you might prefer to run
`npm install leaflet-featuregroup-ogcapi` if you're using a build system, or
you might prefer to manually copy the `Leaflet.FeatureGroup.OGCAPI.js` from
this repository.

### Usage

To instantiate a `L.FeatureGroup.OGCAPI`, specify the base URL of the landing
page for the OGC API, and the **ID** (not the name, not the title) of the
"collection" in the OGC API.

For example:

```js
var overlay = L.featureGroup.ogcApi("https://maps.ecere.com/ogcapi/", {
	collection: "SanDiegoCDB:Trees"
}).addTo(map);
```

Optionally, you can specify:
* A `limit` value (default `100`).
  This is the maximum number of vector features to be loaded at any given time.
* A `padding` value (default `0.1`).
  This is somehow akin to the `padding` of `L.Renderer`s: the percentage
  (default 10%) that the bounding box of data to be loaded extends beyond the
  visible bounds. Larger values means fewer reloads when the user pans around
  the map, but risk hitting the `limit` earlier.
* All options as in `L.GeoJSON`. Specifically, `style`, `pointToLayer` and
  `onEachFeature` are available to style the features and bind interactivity when
  they're spawned.

For example:

```js
var overlay = L.featureGroup.ogcApi("https://maps.ecere.com/ogcapi/", {
	collection: "NaturalEarth:raster:NE1_HR_LC_SR_W_DR",
	limit: 500,
	padding: 0.2,
	onEachFeature: function(feat, layer) {
		layer.bindPopup(feat.id);
	}
}).addTo(map);
```

### Editing features

This implementation covers the not-yet-approed Part 4 of the OGC API Features
specification, as of July 2022. The documentation used is the one at
https://docs.ogc.org/DRAFTS/20-002.html .

The following methods are implemented:

* `createFeature(geoJsonFeature)`
* `replaceFeature(geoJsonFeature)`
* `deleteFeature(geoJsonFeature)`

The following is **not** implemented:

* `updateFeature(geoJsonFeature)`

To create/replace/delete a feature, call the corresponding method, passing the
GeoJSON representation of the feature to it. `replaceFeature` and `deleteFeature`
depend on the GeoJSON representation having an `id` property.

This plugin does not mandate any specific UI for editing features. For a full
example using leaflet-geoman, see https://ivansanchez.gitlab.io/leaflet.featuregroup.ogcapi/demo.html .
Note, in the code for that demo, how the leaflet-geoman events are linked to
FeatureGroup.OGCAPI operations.

This implementation makes assumptions about the REST URLs for creating/replacing/deleting
features. See https://github.com/opengeospatial/ogcapi-features/issues/742 .

### Caveats

Part 2 (CRSs) of the OGC API Features spec is not implemented. This implementation
has only been tested using Leaflet's default L.CRS.EPSG3857` CRS (not
`L.CRS.EPSG4326`, and not `proj4leaflet`). It might not work as expected when
using any other CRS / projection.

Part 3 (CQL) of the OGC API Features spec is not implemented.

When reloading a feature visible on the map, its corresponding Leaflet `L.Layer`
is removed and re-added to the map. This means, among other things, that any
open popups will glitch.

The code is prone to race conditions, specially if fetching the metadata takes a long time (this is a [known bug](https://gitlab.com/IvanSanchez/leaflet.featuregroup.ogcapi/-/issues/2)). In order to alleviate this, you can use:
```js
var overlay = L.featureGroup.ogcApi("https://maps.ecere.com/ogcapi/", {
	collection: "SanDiegoCDB:Trees"
});
overlay.on("ready", ()=>{map.addLayer(overlay);});
```

### Legalese

The code for this plugin is under a Beerware license:

----------------------------------------------------------------------------

"THE BEER-WARE LICENSE":
<ivan@sanchezortega.es> wrote this file. As long as you retain this notice you
can do whatever you want with this stuff. If we meet some day, and you think
this stuff is worth it, you can buy me a beer in return.

----------------------------------------------------------------------------


